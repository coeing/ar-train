// --------------------------------------------------------------------------------------------------------------------
// <copyright file="iTweenPath.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

using UnityEngine;

[AddComponentMenu("Pixelplacement/iTweenPath")]
public class iTweenPath : MonoBehaviour
{
    //public int nodeCount;

    #region Static Fields

    public static Dictionary<string, iTweenPath> paths = new Dictionary<string, iTweenPath>();

    #endregion

    #region Fields
    
    public string initialName = "";

    public bool initialized = false;

    /// <summary>
    ///   Indicates if the path is in local or global coordinate system.
    /// </summary>
    public bool isLocal;

    public List<Vector3> nodes = new List<Vector3> { Vector3.zero, Vector3.zero };

    public Color pathColor = Color.cyan;

    public string pathName = "";

    public bool pathVisible = true;

    #endregion

    #region Public Methods and Operators

    /// <summary>
    ///   Returns the visually edited path as a Vector3 array.
    /// </summary>
    /// <param name="requestedName">
    ///   A <see cref="System.String" /> the requested name of a path.
    /// </param>
    /// <returns>
    ///   A <see cref="Vector3[]" />
    /// </returns>
    //public static Vector3[] GetPath(string requestedName)
    //{
    //    requestedName = requestedName.ToLower();
    //    if (paths.ContainsKey(requestedName))
    //    {
    //        return paths[requestedName].GetPath((Transform)TODO).ToArray();
    //    }
    //    else
    //    {
    //        Debug.Log("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?");
    //        return null;
    //    }
    //}

    /// <summary>
    ///   Returns the reversed visually edited path as a Vector3 array.
    /// </summary>
    /// <param name="requestedName">
    ///   A <see cref="System.String" /> the requested name of a path.
    /// </param>
    /// <returns>
    ///   A <see cref="Vector3[]" />
    /// </returns>
    //public static Vector3[] GetPathReversed(string requestedName)
    //{
    //    requestedName = requestedName.ToLower();
    //    if (paths.ContainsKey(requestedName))
    //    {
    //        List<Vector3> revNodes = paths[requestedName].GetPath((Transform)TODO).GetRange(0, paths[requestedName].GetPath((Transform)TODO).Count);
    //        revNodes.Reverse();
    //        return revNodes.ToArray();
    //    }
    //    else
    //    {
    //        Debug.Log("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?");
    //        return null;
    //    }
    //}

    public List<Vector3> GetPath(Transform root)
    {
        if (!this.isLocal)
        {
            return this.nodes;
        }
        else
        {
            List<Vector3> path = new List<Vector3>();
            foreach (var node in this.nodes)
            {
                Vector3 pathPoint = this.TransformNode(root, node);
                path.Add(pathPoint);
            }
            return path;
        }
    }

    public Vector3 InverseTransformNode(Transform root, Vector3 node)
    {
        if (!this.isLocal)
        {
            return node;
        }
        Vector3 pathPoint = node;
        if (root != null)
        {
            pathPoint = root.TransformPoint(pathPoint);
        }
        pathPoint = this.transform.InverseTransformPoint(pathPoint);
        return pathPoint;
    }

    public Vector3 TransformNode(Transform root, Vector3 node)
    {
        if (!this.isLocal)
        {
            return node;
        }
        Vector3 pathPoint = this.transform.TransformPoint(node);
        if (root != null)
        {
            pathPoint = root.InverseTransformPoint(pathPoint);
        }
        return pathPoint;
    }

    #endregion

    #region Methods

    private void OnDisable()
    {
        paths.Remove(this.pathName.ToLower());
    }

    private void OnDrawGizmos()
    {
        if (this.pathVisible)
        {
            List<Vector3> path = this.GetPath(null);
            if (path.Count > 0)
            {
                iTween.DrawPath(path.ToArray(), this.pathColor);
            }
        }
    }

    private void OnEnable()
    {
        if (!paths.ContainsKey(this.pathName))
        {
            paths.Add(this.pathName.ToLower(), this);
        }
    }

    #endregion
}