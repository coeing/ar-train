﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActionOnTap.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using UnityEngine;

    public abstract class ActionOnTap : MonoBehaviour
    {
        #region Public Methods and Operators

        public Collider Collider;

        private void Start()
        {
            if (this.Collider == null)
            {
                this.Collider = this.collider;
            }
        }

        public void OnTap(TapGesture gesture)
        {
            Vector2 touchPosition = gesture.Position;
            Ray ray = Camera.main.ScreenPointToRay(touchPosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider == this.Collider)
                {
                    this.DoAction(ray.direction, hit.point);
                }
            }
        }

        #endregion

        #region Methods

        protected abstract void DoAction(Vector3 hitDirection, Vector3 hitPoint);

        #endregion
    }
}