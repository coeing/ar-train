﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdjustSpeedOnTap.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using ARTrain.Paths;

    using UnityEngine;

    public class AdjustSpeedOnTap : ActionOnTap
    {
        #region Fields

        /// <summary>
        ///   Indicates if the speed should be set directly or just be adjusted.
        /// </summary>
        public bool IsAbsoluteSpeed;

        public FollowPath Path;

        public float SpeedDelta;

        #endregion

        #region Methods

        protected override void DoAction(Vector3 hitDirection, Vector3 hitPoint)
        {
            if (this.IsAbsoluteSpeed)
            {
                this.Path.Speed = this.SpeedDelta;
            }
            else
            {
                this.Path.AddSpeed(this.SpeedDelta);
            }
        }

        #endregion
    }
}