﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResetGameObject.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using UnityEngine;

    public class ResetGameObject : MonoBehaviour
    {
        #region Fields

        private Vector3 initialPosition;

        private Quaternion initialRotation;

        public GameObject Target;

        #endregion

        #region Public Methods and Operators

        public void OnReset()
        {
            this.Target.transform.localPosition = this.initialPosition;
            this.Target.transform.localRotation = this.initialRotation;
            if (this.Target.rigidbody != null)
            {
                this.Target.rigidbody.velocity = Vector3.zero;
            }
        }

        #endregion

        #region Methods

        private void Start()
        {
            if (this.Target == null)
            {
                this.Target = this.gameObject;
            }

            this.initialPosition = this.Target.transform.localPosition;
            this.initialRotation = this.Target.transform.localRotation;
        }

        #endregion
    }
}