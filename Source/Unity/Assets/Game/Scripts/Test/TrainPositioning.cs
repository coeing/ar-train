﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TrainPositioning.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using ARTrain.Paths;

    using UnityEngine;

    public class TrainPositioning : MonoBehaviour
    {
        #region Fields

        public float AxisDistance = 4;

        public PathConnected BackAxis;

        public PathConnected FrontAxis;

        public GameObject Main;

        public Vector3 MainOffset;

        public FollowPath Path;

        #endregion

        #region Methods

        private void Start()
        {
            Vector3 frontAxisPosition = this.FrontAxis.transform.position;
            Vector3 backAxisPosition = this.BackAxis.transform.position;

            // Get distance between axis.
            this.AxisDistance = (frontAxisPosition - backAxisPosition).magnitude;

            // Get main offset.
            Vector3 mainPosition = (backAxisPosition + frontAxisPosition) * 0.5f;
            this.MainOffset = this.Main.transform.position - mainPosition;
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            // Go back till back axis found.
            const float PercentageStep = 0.0001f;
            float backAxisPercentage = PathUtils.FindDistantPercentage(
                this.FrontAxis.Path, this.FrontAxis.PathPercentage, -PercentageStep, this.AxisDistance);

            // Position axis.
            this.BackAxis.SetPosition(backAxisPercentage);

            // Position main part correctly.
            Vector3 frontAxisPosition = this.FrontAxis.transform.position;
            Vector3 backAxisPosition = this.BackAxis.transform.position;
            Vector3 mainPosition = (backAxisPosition + frontAxisPosition) * 0.5f + this.MainOffset;
            Vector3 mainOrientation = (frontAxisPosition - backAxisPosition).normalized;
            this.Main.transform.position = mainPosition;
            this.Main.transform.rotation = Quaternion.LookRotation(mainOrientation);
        }

        #endregion
    }
}