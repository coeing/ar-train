﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdjustSpeedOnTap.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using ARTrain.Paths;

    using UnityEngine;

    public class StartStopOnTap : ActionOnTap
    {
        #region Fields

        public FollowPath Path;

        public float Speed;

        #endregion

        #region Methods

        protected override void DoAction(Vector3 hitDirection, Vector3 hitPoint)
        {
            this.Path.Speed = this.Path.Speed == 0 ? this.Speed : 0;
        }

        #endregion
    }
}