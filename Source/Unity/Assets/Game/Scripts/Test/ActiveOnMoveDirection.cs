﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveOnMoveDirection.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using ARTrain.Paths;

    using Slash.SystemExt.Utils;

    using UnityEngine;

    public class ActiveOnMoveDirection : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   If move direction None is considered.
        /// </summary>
        public bool CheckNone;

        /// <summary>
        ///   When is the target active.
        /// </summary>
        public MoveDirection MoveDirection;

        public FollowPath Path;

        public GameObject Target;

        #endregion

        #region Methods

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            MoveDirection currentMoveDirection = this.Path.MoveDirection;
            if (currentMoveDirection != MoveDirection.None)
            {
                this.Target.SetActive(this.MoveDirection.IsOptionSet(currentMoveDirection));
            }
            else if (this.CheckNone)
            {
                this.Target.SetActive(false);
            }
        }

        #endregion
    }
}