﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReloadBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using UnityEngine;

    public class ReloadBehaviour : MonoBehaviour
    {
        #region Public Methods and Operators

        public void OnReload()
        {
            Application.LoadLevel(Application.loadedLevel);
        }

        #endregion
    }
}