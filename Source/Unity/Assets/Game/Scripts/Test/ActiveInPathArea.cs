﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveInPathArea.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using ARTrain.Paths;

    using UnityEngine;

    /// <summary>
    ///   Makes a game object only active if inside path area.
    /// </summary>
    public class ActiveInPathArea : MonoBehaviour
    {
        #region Fields

        public PathArea PathArea;

        public GameObject Target;

        private bool isStarted;

        #endregion

        #region Methods

        private void InitialSetup()
        {
            // Initial setup.
            if (this.PathArea.InsideArea)
            {
                this.OnEntered();
            }
            else
            {
                this.OnLeft();
            }
        }

        private void OnDisable()
        {
            // Remove from events.
            this.PathArea.Entered -= this.OnEntered;
            this.PathArea.Left -= this.OnLeft;
        }

        private void OnEnable()
        {
            // Register for events.
            this.PathArea.Entered += this.OnEntered;
            this.PathArea.Left += this.OnLeft;

            if (this.isStarted)
            {
                this.InitialSetup();
            }
        }

        private void OnEntered()
        {
            this.Target.SetActive(true);
        }

        private void OnLeft()
        {
            this.Target.SetActive(false);
        }

        private void Start()
        {
            this.isStarted = true;
            this.InitialSetup();
        }

        #endregion
    }
}