﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplyForceOnTap.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using UnityEngine;

    public class ApplyForceOnTap : ActionOnTap
    {
        #region Fields

        public float Force = 1000;

        #endregion

        #region Methods

        protected override void DoAction(Vector3 hitDirection, Vector3 hitPoint)
        {
            this.rigidbody.AddForceAtPosition(hitDirection * this.Force, hitPoint);
        }

        #endregion
    }
}