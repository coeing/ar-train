// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DisableRigidbodiesIfNotTracked.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using UnityEngine;

    public class DisableRigidbodiesIfNotTracked : MonoBehaviour, ITrackableEventHandler
    {
        #region Fields

        private TrackableBehaviour trackableBehaviour;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Implementation of the ITrackableEventHandler function called when the
        ///   tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
            TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED
                || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                this.OnTrackingFound();
            }
            else
            {
                this.OnTrackingLost();
            }
        }

        #endregion

        #region Methods

        private void OnTrackingFound()
        {
            Rigidbody[] rigidbodyComponents = this.GetComponentsInChildren<Rigidbody>(true);

            // Enable rigid bodies:
            foreach (Rigidbody component in rigidbodyComponents)
            {
                component.WakeUp();
            }
        }

        private void OnTrackingLost()
        {
            Rigidbody[] rigidbodyComponents = this.GetComponentsInChildren<Rigidbody>(true);

            // Disable rigid bodies:
            foreach (Rigidbody component in rigidbodyComponents)
            {
                component.Sleep();
            }
        }

        private void Start()
        {
            this.trackableBehaviour = this.GetComponent<TrackableBehaviour>();
            if (this.trackableBehaviour)
            {
                this.trackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion
    }
}