﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StopAtPositionOnTap.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using ARTrain.Paths;

    using Slash.SystemExt.Utils;

    using UnityEngine;

    public class StopAtPositionOnTap : ActionOnTap
    {
        #region Fields

        /// <summary>
        ///   Direction of movement to stop.
        /// </summary>
        public MoveDirection Direction = MoveDirection.Both;

        public StopPoint StopPoint;

        public FollowPath Vehicle;

        #endregion

        #region Methods

        protected override void DoAction(Vector3 hitDirection, Vector3 hitPoint)
        {
            Debug.Log("Should stop");
            this.StopPoint.enabled = true;
        }

        private void OnDisable()
        {
            this.StopPoint.Stopped -= this.OnStopped;
        }

        private void OnEnable()
        {
            this.StopPoint.enabled = false;
            this.StopPoint.Stopped += this.OnStopped;
        }

        private void OnStopped()
        {
            this.StopPoint.enabled = false;
        }

        private void Update()
        {
            if (!this.StopPoint.enabled)
            {
                return;
            }

            // Check if desired direction still correct.
            if (!this.Direction.IsOptionSet(this.Vehicle.MoveDirection))
            {
                this.StopPoint.enabled = false;
            }
        }

        #endregion
    }
}