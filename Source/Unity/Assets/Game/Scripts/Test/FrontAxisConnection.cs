﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrontAxisConnection.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using ARTrain.Paths;

    using Slash.Math.Utils;

    using UnityEngine;

    public class FrontAxisConnection : MonoBehaviour
    {
        #region Fields

        public float ConnectorFrontAxisDistance;

        public GameObject ConnectorOther;

        public GameObject ConnectorSelf;

        public PathConnected FrontAxis;

        public PathConnected BackAxisOther;

        public float closestPoint;

        public Vector3 connectorPoint;

        #endregion

        #region Public Methods and Operators

        public void Start()
        {
            this.ConnectorFrontAxisDistance =
                MathUtils.Abs(this.FrontAxis.transform.position.z - this.ConnectorSelf.transform.position.z);
        }

        public void Update()
        {
            this.ConnectorSelf.transform.position = this.ConnectorOther.transform.position;
            this.UpdatePosition();
        }

        public void UpdatePosition()
        {
            // Get nearest path position to connector.
            this.connectorPoint = this.ConnectorSelf.transform.position;
            const float PercentageStep = 0.00001f;
            this.closestPoint = PathUtils.GetClosestPercentage(
                this.FrontAxis.Path,
                this.ConnectorSelf.transform.position,
                this.BackAxisOther.PathPercentage,
                -PercentageStep, false);

            // Move backwards till distance between connector and axis reached.
            float frontAxisPercentage = PathUtils.FindDistantPercentage(
                this.FrontAxis.Path,
                closestPoint,
                -PercentageStep,
                this.ConnectorFrontAxisDistance);
            this.FrontAxis.SetPosition(frontAxisPercentage);
        }

        #endregion
    }
}