﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbsolutePathPoint.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    public class AbsolutePathPoint : PathPoint
    {
        #region Methods

        private void Start()
        {
            this.PathPercentage = PathUtils.GetClosestPercentage(this.Path, this.transform.position);
        }

        #endregion
    }
}