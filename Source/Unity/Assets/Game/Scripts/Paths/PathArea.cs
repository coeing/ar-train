﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PathArea.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using Slash.Math.Utils;

    using UnityEngine;

    public sealed class PathArea : MonoBehaviour
    {
        #region Fields

        public Transform AreaEnd;

        public RelativePathPoint AreaEndPoint;

        public Transform AreaStart;

        public RelativePathPoint AreaStartPoint;

        public Path Path;

        public PathConnected Target;

        private float areaEndPercentage;

        private float areaStartPercentage;

        private bool isStarted;

        #endregion

        #region Delegates

        public delegate void EnteredDelegate();

        public delegate void LeftDelegate();

        #endregion

        #region Public Events

        public event EnteredDelegate Entered;

        public event LeftDelegate Left;

        #endregion

        #region Public Properties

        public bool InsideArea { get; private set; }

        #endregion

        #region Methods

        private void CheckInsideArea()
        {
            float pathPercentage = this.Target.PathPercentage;
            bool newInsideArea = MathUtils.IsWithinBoundsInclusive(
                pathPercentage, this.areaStartPercentage, this.areaEndPercentage);
            if (newInsideArea != this.InsideArea)
            {
                if (this.InsideArea)
                {
                    // Area left.
                    this.OnLeft();
                }
                else
                {
                    // Area entered.
                    this.OnEntered();
                }
                this.InsideArea = newInsideArea;
            }
        }

        private void OnDisable()
        {
            if (this.InsideArea)
            {
                this.OnLeft();
                this.InsideArea = false;
            }
        }

        private void OnEnable()
        {
            if (this.isStarted)
            {
                // Initial setup.
                this.CheckInsideArea();
            }
        }

        private void OnEntered()
        {
            EnteredDelegate handler = this.Entered;
            if (handler != null)
            {
                handler();
            }
        }

        private void OnLeft()
        {
            LeftDelegate handler = this.Left;
            if (handler != null)
            {
                handler();
            }
        }
        
        private void Start()
        {
            this.isStarted = true;

            // Get path position for area start and end.
            if (this.AreaStart != null)
            {
                this.areaStartPercentage = PathUtils.GetClosestPercentage(this.Path, this.AreaStart.position, false);
            }
            else if (this.AreaStartPoint != null)
            {
                this.areaStartPercentage = this.AreaStartPoint.PathPercentage;
            }

            if (this.AreaEnd != null)
            {
                this.areaEndPercentage = PathUtils.GetClosestPercentage(this.Path, this.AreaEnd.position, false);
            }
            else if (this.AreaEndPoint != null)
            {
                this.areaEndPercentage = this.AreaEndPoint.PathPercentage;
            }

            if (this.areaStartPercentage > this.areaEndPercentage)
            {
                MathUtils.Swap(ref this.areaStartPercentage, ref this.areaEndPercentage);
            }

            this.CheckInsideArea();
        }

        private void Update()
        {
            this.CheckInsideArea();
        }

        #endregion
    }
}