﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FollowPath.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using Slash.Math.Utils;

    using UnityEngine;

    public class FollowPath : MonoBehaviour
    {
        #region Fields

        public float MaxSpeed = 10.0f;

        public int MoveApproximations = 10;

        public Path Path;

        public float Speed = 1.0f;

        /// <summary>
        ///   Where to start (0.0 - 1.0).
        /// </summary>
        [Range(0, 1)]
        public float StartPercentage;

        /// <summary>
        ///   Indicates if the target should stop if path end was reached.
        /// </summary>
        public bool StopAtPathEnd;

        public PathConnected Target;

        /// <summary>
        ///   Indicates if the rotation of the target should be updated.
        /// </summary>
        public bool UpdateRotation;

        #endregion

        #region Public Properties

        public MoveDirection MoveDirection
        {
            get
            {
                return this.Speed > 0
                           ? MoveDirection.Forward
                           : (this.Speed == 0 ? MoveDirection.None : MoveDirection.Backward);
            }
        }

        #endregion

        #region Public Methods and Operators

        public void AddSpeed(float delta)
        {
            this.Speed = MathUtils.Clamp(this.Speed + delta, -this.MaxSpeed, this.MaxSpeed);
        }

        public void FixedUpdate()
        {
            // Don't move if no speed.
            if (this.Speed == 0)
            {
                return;
            }

            // Don't move if at path end.
            bool isAtPathEnd = this.Target.PathPercentage <= 0.0f && this.Speed < 0
                               || this.Target.PathPercentage >= 1.0f && this.Speed > 0;
            if (isAtPathEnd && this.StopAtPathEnd)
            {
                this.Speed = 0;
                return;
            }

            //float realPercentToMove = (this.Speed * Time.deltaTime) / this.Path.PathLength;

            float distanceToMove = this.Speed * Time.fixedDeltaTime;
            float percentageToMove = distanceToMove / this.Path.PathLength;

            // Approximate real percentage to move.
            float realPercentageToMove = percentageToMove;
            float approximationStep = percentageToMove;
            Vector3 position = this.Target.transform.position;
            int i = 0;
            float bestPathPercentage = this.Target.PathPercentage + realPercentageToMove;
            float bestDistanceApproximation = float.MaxValue;
            do
            {
                float newPathPercentage = this.Target.PathPercentage + realPercentageToMove;
                newPathPercentage = this.StopAtPathEnd
                                        ? MathUtils.Saturate(newPathPercentage)
                                        : MathUtils.Wrap(newPathPercentage);
                Vector3 newPosition = this.Path.Interp(newPathPercentage);
                float distance = (newPosition - position).magnitude;
                float distanceDiff = MathUtils.Abs(distance - MathUtils.Abs(distanceToMove));
                if (distanceDiff < bestDistanceApproximation)
                {
                    bestDistanceApproximation = distanceDiff;
                    bestPathPercentage = newPathPercentage;
                }

                if (distance < MathUtils.Abs(distanceToMove))
                {
                    realPercentageToMove += approximationStep;
                }
                else
                {
                    realPercentageToMove -= approximationStep;
                }

                approximationStep *= 0.5f;
                ++i;
            }
            while (i < this.MoveApproximations);

            this.UpdateTarget(bestPathPercentage);
        }

        public void Start()
        {
            // Update position.
            this.Target.PathPercentage = this.StartPercentage;
        }

        public void UpdateTarget(float pathPercentage)
        {
            // Update position.
            this.Target.SetPosition(pathPercentage);

            if (this.UpdateRotation)
            {
                // Update rotation.
                float lookAheadPercentage = pathPercentage + iTween.Defaults.lookAhead * MathUtils.Sign(this.Speed);
                lookAheadPercentage = this.StopAtPathEnd
                                          ? MathUtils.Saturate(lookAheadPercentage)
                                          : MathUtils.Wrap(lookAheadPercentage);
                if (lookAheadPercentage != pathPercentage)
                {
                    Vector3 lookAheadPosition = this.Path.Interp(lookAheadPercentage);
                    iTween.LookUpdate(this.Target.gameObject, lookAheadPosition, 0);
                }
            }
        }

        #endregion
    }
}