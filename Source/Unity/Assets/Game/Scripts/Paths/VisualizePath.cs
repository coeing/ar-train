﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VisualizePath.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using System.Collections.Generic;

    using UnityEngine;

    using Vectrosity;

    public class VisualizePath : MonoBehaviour
    {
        #region Fields

        public Color Color = Color.magenta;

        public Material LineMaterial;

        public float LineWidth = 1.0f;

        public Path Path;

        public float SampleStep = 0.01f;

        private VectorLine line;

        #endregion

        #region Public Methods and Operators

        public void Update()
        {
            if (this.line != null)
            {
                return;
            }

            float t = 0;
            List<Vector3> points = new List<Vector3>();
            while (t < 1.0f)
            {
                Vector3 point = this.Path.transform.TransformPoint(this.Path.Interp(t));
                points.Add(point);
                t += this.SampleStep;
            }

            this.line = new VectorLine(
                "Path", points.ToArray(), this.Color, this.LineMaterial, this.LineWidth, LineType.Continuous);
            this.line.Draw3DAuto();
            this.line.AddNormals();
            this.line.vectorObject.transform.parent = this.transform;
        }

        #endregion
    }
}