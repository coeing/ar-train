﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PathUtils.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using Slash.Math.Utils;

    using UnityEngine;

    public static class PathUtils
    {
        #region Public Methods and Operators

        public static float FindDistantPercentage(
            Path path, float startPercentage, float percentageStep, float desiredDistance)
        {
            if (path == null)
            {
                return startPercentage;
            }
            return FindDistantPercentage(
                path, startPercentage, percentageStep, desiredDistance, path.Interp(startPercentage));
        }

        public static float FindDistantPercentage(
            Path path, float startPercentage, float percentageStep, float desiredDistance, Vector3 targetPosition)
        {
            if (path == null)
            {
                return startPercentage;
            }

            float distance = 0;
            float percentage = startPercentage;
            while (distance < desiredDistance)
            {
                Vector3 distantPosition = path.Interp(percentage);
                distance = (targetPosition - distantPosition).magnitude;
                if (distance >= desiredDistance)
                {
                    return percentage;
                }

                percentage = MathUtils.Wrap(percentage + percentageStep);
            }
            return percentage;
        }

        public static float GetClosestPercentage(Path path, Vector3 point, bool checkHeight = true)
        {
            if (path == null || path.Points.Length == 0)
            {
                return 0;
            }

            if (path.Points.Length == 1)
            {
                return 0;
            }

            // Get closest segment.
            float closestSegmentDistance = float.MaxValue;
            int closestSegment = -1;
            int numSections = path.Points.Length;
            for (int i = 0; i < numSections - 1; ++i)
            {
                Vector3 lineStart = path.Points[i];
                Vector3 lineEnd = path.Points[i + 1];

                Vector3 u = lineEnd - lineStart;
                Vector3 pq = point - lineStart;

                float distance = Vector3.Cross(pq, u).magnitude / u.magnitude;
                if (distance < closestSegmentDistance)
                {
                    closestSegment = i;
                    closestSegmentDistance = distance;
                }
            }

            // Get closest point on path.
            float percentage = MathUtils.Wrap((closestSegment - 1) * 1.0f / numSections);
            const float PercentageStep = 0.00001f;
            return GetClosestPercentage(path, point, percentage, PercentageStep, checkHeight);
        }

        public static float GetClosestPercentage(
            Path path, Vector3 point, float percentage, float percentageStep, bool checkHeight = true)
        {
            float closestPercentage = percentage;
            float closestPointPathDistance = float.MaxValue;
            float checkedPath = 0;
            while (checkedPath < 1.0f)
            {
                // Check percentage.
                Vector3 pathPosition = path.Interp(percentage);
                float pointPathDistance = checkHeight
                                              ? (pathPosition - point).magnitude
                                              : (new Vector2(pathPosition.x, pathPosition.z)
                                                 - new Vector2(point.x, point.z)).magnitude;
                if (pointPathDistance < closestPointPathDistance)
                {
                    closestPointPathDistance = pointPathDistance;
                    closestPercentage = percentage;
                }
                else if (closestPointPathDistance < 1.0f)
                {
                    break;
                }

                if (path.CircularPath)
                {
                    percentage = MathUtils.Wrap(percentage + percentageStep);
                    checkedPath += MathUtils.Abs(percentageStep);
                }
                else
                {
                    // For a non-circular path make sure that start and end points are checked.
                    if (percentage < 1.0f)
                    {
                        float newPercentage = percentage + percentageStep;
                        if (newPercentage > 1.0f)
                        {
                            newPercentage = 1.0f;
                            checkedPath += MathUtils.Abs(1.0f - percentage);
                        }
                        else
                        {
                            checkedPath += MathUtils.Abs(percentageStep);
                        }
                        percentage = newPercentage;
                    }
                    else if (checkedPath < 1.0f)
                    {
                        // Wrap once.
                        percentage = 0.0f;
                    }
                }
            }

            return closestPercentage;
        }

        #endregion
    }
}