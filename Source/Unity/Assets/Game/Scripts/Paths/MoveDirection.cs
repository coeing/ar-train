﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MoveDirection.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    public enum MoveDirection
    {
        None,

        Forward = 0x1,

        Backward = 0x2,

        Both = Forward | Backward,
    }
}