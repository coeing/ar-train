﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StopPoint.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using System.Collections;

    using Slash.Math.Utils;

    using UnityEngine;

    public class StopPoint : MonoBehaviour
    {
        #region Fields

        public bool ContinueOnDisable;

        public FollowPath Engine;

        public PathPoint PathPoint;

        /// <summary>
        ///   Delay before the vehicle starts again after a stop.
        /// </summary>
        public float RestartDelay = 1.0f;

        public PathConnected Vehicle;

        private float lastPercentageDiff;

        private float pathPointPercentage;

        private float speedBeforeStop;

        private bool wasStopped;

        #endregion

        #region Delegates

        public delegate void StoppedDelegate();

        #endregion

        #region Public Events

        public event StoppedDelegate Stopped;

        #endregion

        #region Methods

        private void OnDisable()
        {
            if (this.ContinueOnDisable && this.wasStopped)
            {
                this.wasStopped = false;
                this.Vehicle.StartCoroutine(this.Restart());
            }
        }

        private void OnEnable()
        {
            this.lastPercentageDiff = MathUtils.Wrap(this.pathPointPercentage - this.Vehicle.PathPercentage);
            this.wasStopped = false;
        }

        private void OnPassedStopPoint()
        {
            // Stop.
            this.speedBeforeStop = this.Engine.Speed;
            this.Engine.Speed = 0;
            this.wasStopped = true;

            this.OnStopped();
        }

        private void OnStopped()
        {
            StoppedDelegate handler = this.Stopped;
            if (handler != null)
            {
                handler();
            }
        }

        private IEnumerator Restart()
        {
            yield return new WaitForSeconds(this.RestartDelay);

            // Check if stopped again.
            if (!this.wasStopped)
            {
                this.Engine.Speed = this.speedBeforeStop;
            }
        }

        private void Start()
        {
            this.pathPointPercentage = this.PathPoint.PathPercentage;
            this.lastPercentageDiff = MathUtils.Wrap(this.pathPointPercentage - this.Vehicle.PathPercentage);
        }

        private void Update()
        {
            float pathPercentage = this.Vehicle.PathPercentage;
            float percentageDiff = MathUtils.Wrap(this.pathPointPercentage - pathPercentage);
            if (this.Engine.Speed > 0 && percentageDiff > this.lastPercentageDiff)
            {
                Debug.Log("Passed: " + this.lastPercentageDiff + ", " + percentageDiff, this);
                this.OnPassedStopPoint();
            }
            else if (this.Engine.Speed < 0 && percentageDiff < this.lastPercentageDiff)
            {
                this.OnPassedStopPoint();
            }
            this.lastPercentageDiff = percentageDiff;
        }

        #endregion
    }
}