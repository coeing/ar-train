﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Path.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using System.Collections.Generic;
    using System.Linq;

    using UnityEngine;

    public class Path : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Indicates if the designed path should be circular.
        /// </summary>
        public bool CircularPath;

        public iTweenPath PathDesign;

        private Vector3[] spline;

        #endregion

        #region Public Properties

        public float PathLength { get; private set; }

        public Vector3[] Points { get; private set; }

        #endregion

        #region Public Methods and Operators

        public Vector3 Interp(float percentage)
        {
            return this.transform.TransformPoint(iTween.Interp(this.spline, percentage));
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Called before first Update call.
        /// </summary>
        private void Awake()
        {
            List<Vector3> nodes = new List<Vector3>(this.PathDesign.GetPath(this.transform));

            if (this.CircularPath)
            {
                // Close path.
                Vector3 start = nodes.First();
                nodes.Add(start);
            }

            this.Points = nodes.ToArray();
            this.spline = iTween.PathControlPointGenerator(this.Points);
            this.PathLength = iTween.PathLength(this.Points);
        }

        #endregion

        public Vector3[] GetSpline()
        {
            List<Vector3> nodes = new List<Vector3>(this.PathDesign.GetPath(null));

            if (this.CircularPath)
            {
                // Close path.
                Vector3 start = nodes.First();
                nodes.Add(start);
            }

            return iTween.PathControlPointGenerator(nodes.ToArray());
        }
    }
}