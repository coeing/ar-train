﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PathConnected.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using UnityEngine;

    public class PathConnected : MonoBehaviour
    {
        #region Fields
        
        public Path Path;

        public float PathPercentage;

        #endregion

        #region Public Methods and Operators

        public void SetPosition(float pathPercentage)
        {
            this.PathPercentage = pathPercentage;

            if (this.Path != null)
            {
                // Get path position.
                Vector3 pathPosition = this.Path.Interp(pathPercentage);
                this.transform.position = pathPosition;
            }
        }
        
        #endregion
    }
}