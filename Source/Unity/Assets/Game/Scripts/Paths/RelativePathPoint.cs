﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RelativePathPoint.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using System.Collections.Generic;

    using UnityEngine;

    [ExecuteInEditMode]
    public class RelativePathPoint : PathPoint
    {
        #region Public Methods and Operators

        public void Update()
        {
            if (this.Path != null)
            {
                this.transform.position = iTween.Interp(this.Path.GetSpline(), this.PathPercentage);
            }
        }

        #endregion
    }
}