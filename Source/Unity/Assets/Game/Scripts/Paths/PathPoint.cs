﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PathPoint.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Paths
{
    using UnityEngine;

    public class PathPoint : MonoBehaviour
    {
        #region Fields

        public Path Path;

        [Range(0, 1)]
        public float PathPercentage;

        #endregion
    }
}