﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResetPathOnTracked.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Test
{
    using System.Collections;

    using ARTrain.Paths;

    using UnityEngine;

    public class ResetPathOnTracked : MonoBehaviour, ITrackableEventHandler
    {
        #region Fields

        public float DelayStart;

        public FollowPath Engine;

        private float initialSpeed;

        private TrackableBehaviour trackableBehaviour;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Implementation of the ITrackableEventHandler function called when the
        ///   tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
            TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED
                || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                this.OnTrackingFound();
            }
        }

        #endregion

        #region Methods

        private IEnumerator DelayedStart()
        {
            this.Engine.Speed = 0;

            yield return new WaitForSeconds(this.DelayStart);

            this.Engine.Speed = this.initialSpeed;
        }

        private void OnTrackingFound()
        {
            if (this.Engine != null)
            {
                // Update position.
                this.Engine.UpdateTarget(this.Engine.StartPercentage);
                this.StartCoroutine(this.DelayedStart());
            }
        }

        private void Start()
        {
            this.trackableBehaviour = this.GetComponent<TrackableBehaviour>();
            if (this.trackableBehaviour)
            {
                this.trackableBehaviour.RegisterTrackableEventHandler(this);
            }

            if (this.Engine != null)
            {
                this.initialSpeed = this.Engine.Speed;
            }
        }

        #endregion
    }
}