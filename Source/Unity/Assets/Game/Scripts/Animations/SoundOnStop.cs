﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SoundOnStop.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Animations
{
    using ARTrain.Paths;

    using UnityEngine;

    public class SoundOnStop : MonoBehaviour
    {
        #region Fields

        public AudioClip AudioClip;

        public AudioSource AudioSource;

        public float Delay;

        public FollowPath Vehicle;

        private float lastSpeed;

        #endregion

        #region Methods

        private void OnVehicleStopped()
        {
            if (this.AudioClip == null)
            {
                return;
            }

            if (this.AudioSource == null)
            {
                this.AudioSource = this.gameObject.AddComponent<AudioSource>();
            }

            this.AudioSource.clip = this.AudioClip;
            if (this.Delay < 0)
            {
                this.AudioSource.time = -this.Delay;
            }
            this.AudioSource.Play();
        }

        private void Update()
        {
            if (this.Vehicle.Speed != this.lastSpeed)
            {
                this.lastSpeed = this.Vehicle.Speed;
                if (this.lastSpeed == 0)
                {
                    this.OnVehicleStopped();
                }
            }
        }

        #endregion
    }
}