﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScaleAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Animations
{
    using System.Collections;

    using UnityEngine;

    public class ScaleAnimation : PlayAnimation
    {
        #region Fields

        public Vector3 To = Vector3.one;

        #endregion

        #region Public Methods and Operators

        #endregion

        #region Methods

        #endregion

        protected override void StartAnimation(Hashtable args)
        {
            args["scale"] = this.To;
            iTween.ScaleTo(this.Target, args);
        }

        protected override void StartEndAnimation(Hashtable args)
        {
            args["scale"] = Vector3.one;
            iTween.ScaleTo(this.Target, args);
        }
    }
}