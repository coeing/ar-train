﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RotateAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Animations
{
    using System.Collections;

    using UnityEngine;

    public class RotateAnimation : PlayAnimation
    {
        #region Fields

        public Vector3 Rotation;

        private Vector3 initialRotation;

        #endregion

        #region Methods

        protected override void StartAnimation(Hashtable args)
        {
            Debug.Log(
                "Start rotate animation to " + this.Rotation + ", current: " + this.Target.transform.localEulerAngles);

            args["islocal"] = true;
            args["rotation"] = this.Rotation;
            iTween.RotateTo(this.Target, args);
        }

        protected override void StartEndAnimation(Hashtable args)
        {
            Debug.Log("Start rotate end animation for " + this.Target + " to " + this.initialRotation, this);

            args["islocal"] = true;
            args["rotation"] = this.initialRotation;
            iTween.RotateTo(this.Target, args);
        }

        private void Start()
        {
            this.initialRotation = this.Target.transform.localEulerAngles;
        }

        #endregion
    }
}