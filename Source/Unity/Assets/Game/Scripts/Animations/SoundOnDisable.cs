﻿namespace ARTrain.Animations
{
    using UnityEngine;

    public class SoundOnDisable : MonoBehaviour
    {
        #region Methods

        public AudioClip AudioClip;

        public AudioSource AudioSource;

        public float Delay;

        private void OnDisable()
        {
            if (this.AudioClip == null)
            {
                return;
            }

            if (this.AudioSource == null)
            {
                this.AudioSource = this.gameObject.AddComponent<AudioSource>();
            }

            this.AudioSource.clip = this.AudioClip;
            if (this.Delay < 0)
            {
                this.AudioSource.time = -this.Delay;
            }
            this.AudioSource.Play();
        }

        #endregion
    }
}
