﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Animations
{
    using System.Collections;

    using UnityEngine;

    public abstract class PlayAnimation : MonoBehaviour
    {
        #region Fields

        public float Duration = 1.0f;

        public iTween.EaseType EaseType;

        /// <summary>
        ///   Indicates if after the first loop should be stopped.
        /// </summary>
        public bool LoopOnlyOnce;

        public iTween.LoopType LoopType;

        /// <summary>
        ///   If animation should be started when behaviour gets enabled.
        /// </summary>
        public bool StartOnEnable;

        public GameObject Target;

        private int completeCount;

        private bool isPlaying;

        #endregion

        #region Delegates

        public delegate void StartedDelegate();

        public delegate void StoppedDelegate();

        #endregion

        #region Public Events

        public event StartedDelegate Started;

        public event StoppedDelegate Stopped;

        #endregion

        #region Public Methods and Operators

        public void StartAnimation()
        {
            if (this.isPlaying)
            {
                return;
            }

            iTween.Stop(this.Target);
            Hashtable args = this.CreateTweenArgs();
            args["onstarttarget"] = this.gameObject;
            args["onstart"] = "OnAnimationStart";
            args["oncompletetarget"] = this.gameObject;
            args["oncomplete"] = "OnAnimationComplete";
            this.StartAnimation(args);

            this.isPlaying = true;
            this.completeCount = 0;

            this.OnStarted();
        }

        public void StopAnimation()
        {
            // Stop old animation and start new to normal scale.
            iTween.Stop(this.Target);
            Hashtable args = this.CreateTweenArgs();
            args["looptype"] = iTween.LoopType.none;
            this.StartEndAnimation(args);

            this.isPlaying = false;
            this.OnStopped();
        }

        #endregion

        #region Methods

        protected virtual void OnAnimationComplete()
        {
            Debug.Log("On animation complete", this.gameObject);
            ++this.completeCount;
            if (this.LoopOnlyOnce && this.completeCount >= 2)
            {
                this.StopAnimation();
            }
        }

        protected virtual void OnAnimationStart()
        {
            Debug.Log("On animation start", this.gameObject);
        }

        protected virtual void OnStarted()
        {
            StartedDelegate handler = this.Started;
            if (handler != null)
            {
                handler();
            }
        }

        protected virtual void OnStopped()
        {
            StoppedDelegate handler = this.Stopped;
            if (handler != null)
            {
                handler();
            }
        }

        protected abstract void StartAnimation(Hashtable args);

        protected abstract void StartEndAnimation(Hashtable args);

        private Hashtable CreateTweenArgs()
        {
            Hashtable args = new Hashtable();
            args["time"] = this.Duration;
            args["easetype"] = this.EaseType;
            args["looptype"] = this.LoopType;
            return args;
        }

        private void OnDisable()
        {
            this.StopAnimation();
        }

        private void OnEnable()
        {
            if (this.StartOnEnable)
            {
                this.StartAnimation();
            }
        }

        private void Start()
        {
            if (this.Target == null)
            {
                this.Target = this.gameObject;
            }
        }

        #endregion
    }
}