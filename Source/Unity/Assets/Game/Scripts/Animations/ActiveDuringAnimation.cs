﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveDuringAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Animations
{
    using UnityEngine;

    public class ActiveDuringAnimation : MonoBehaviour
    {
        #region Fields

        public PlayAnimation Animation;

        public bool Invert;

        public GameObject Target;

        #endregion

        #region Methods

        private void OnDisable()
        {
            this.Animation.Started -= this.OnStarted;
            this.Animation.Stopped -= this.OnStopped;
        }

        private void OnEnable()
        {
            this.Animation.Started += this.OnStarted;
            this.Animation.Stopped += this.OnStopped;
        }

        private void OnStarted()
        {
            this.Target.SetActive(!this.Invert);
        }

        private void OnStopped()
        {
            this.Target.SetActive(this.Invert);
        }

        private void Start()
        {
            if (this.Target == null)
            {
                this.Target = this.gameObject;
            }
        }

        #endregion
    }
}