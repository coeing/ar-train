﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FlipModels.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Animations
{
    using UnityEngine;

    public class FlipModels : MonoBehaviour
    {
        #region Fields

        public float Frequency;

        public GameObject ModelA;

        public GameObject ModelB;

        private float remainingTime;

        #endregion

        #region Public Methods and Operators

        public void Start()
        {
            this.remainingTime = this.Frequency != 0 ? 1 / this.Frequency : float.MaxValue;
        }

        public void Update()
        {
            this.remainingTime -= Time.deltaTime;
            if (this.remainingTime <= 0)
            {
                this.Flip();
                this.remainingTime = this.Frequency != 0 ? 1 / this.Frequency : float.MaxValue;
            }
        }

        #endregion

        #region Methods

        private void Flip()
        {
            bool modelAEnabled = !this.ModelA.activeSelf;
            this.ModelA.SetActive(modelAEnabled);
            this.ModelB.SetActive(!modelAEnabled);
        }

        #endregion
    }
}