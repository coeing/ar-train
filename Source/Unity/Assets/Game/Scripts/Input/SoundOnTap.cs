﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SoundOnTap.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Input
{
    using ARTrain.Test;

    using UnityEngine;

    public class SoundOnTap : ActionOnTap
    {
        #region Fields

        public AudioClip AudioClip;

        public AudioSource AudioSource;

        public float Delay;

        #endregion

        #region Methods

        protected override void DoAction(Vector3 hitDirection, Vector3 hitPoint)
        {
            Debug.Log("Play sound");
            if (this.AudioClip == null)
            {
                return;
            }

            if (this.AudioSource == null)
            {
                this.AudioSource = this.gameObject.AddComponent<AudioSource>();
            }

            this.AudioSource.clip = this.AudioClip;
            if (this.Delay < 0)
            {
                this.AudioSource.time = -this.Delay;
            }
            this.AudioSource.Play();
        }

        #endregion
    }
}