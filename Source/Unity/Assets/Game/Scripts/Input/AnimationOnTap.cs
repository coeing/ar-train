﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AnimationOnTap.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Input
{
    using ARTrain.Animations;
    using ARTrain.Test;

    using UnityEngine;

    public class AnimationOnTap : ActionOnTap
    {
        #region Fields

        public PlayAnimation Animation;

        #endregion

        #region Methods

        protected override void DoAction(Vector3 hitDirection, Vector3 hitPoint)
        {
            this.Animation.StartAnimation();
        }

        #endregion
    }
}