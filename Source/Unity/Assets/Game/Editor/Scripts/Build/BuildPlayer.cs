﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BuildPlayer.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ARTrain.Unity.Editor.Build
{
    using Slash.Unity.Editor.Common.Build;

    public static class BuildPlayer
    {
        #region Public Methods and Operators

        /// <summary>
        ///   Perform a build from command line.
        /// </summary>
        public static void PerformBuild()
        {
            BuildManager buildManager = new BuildManager();
            buildManager.PerformBuild();
        }

        #endregion
    }
}