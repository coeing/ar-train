﻿// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.
//
// Added alpha mask texture to support semi-transparent areas.

Shader "Mobile/Diffuse Alpha Mask" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
    _AlphaTex ("Alpha mask (R)", 2D) = "white" {}
}
SubShader {
	Tags { "Queue"="Transparent" "RenderType"="Transparent" }
	LOD 150
	
    Blend SrcAlpha OneMinusSrcAlpha 
	
	CGPROGRAM
	#pragma surface surf Lambert noforwardadd

	sampler2D _MainTex;
	sampler2D _AlphaTex;

	struct Input {
		float2 uv_MainTex;
	};

	void surf (Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
		fixed4 col2 = tex2D(_AlphaTex, IN.uv_MainTex);
		o.Albedo = c.rgb;
		o.Alpha = col2.r;
	}
	ENDCG
}

Fallback "Mobile/VertexLit"
}
